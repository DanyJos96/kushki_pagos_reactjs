import React, {useState} from "react";
import styles from "./Payment.module.css"
import {useForm, Controller} from "react-hook-form";
import LogoKushki from "../img/Logo Kushki - Oscuro.svg";
import {Kushki} from "@kushki/js";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import CircularProgress from "@material-ui/core/CircularProgress";
import InputMask from 'react-input-mask';


const chargeAmount = 58.99;
const chargeCurrency = "USD";
const KUSHKI_PUBLIC_MERCHANT_ID = "9200f0d1c3cc457bbc51742a94e28486";


export function PaymentForm() {
    const [token, setToken] = useState(undefined);
    const {control, handleSubmit, formState: {errors}, reset, watch, register} = useForm({
        defaultValues: {
            cardName: '',
            cardNumber: '',
            expDate: '',
            cvc: ''
        }
    });

    const [responseSuccess, setResponseSuccess] = useState(undefined);
    const [responseError, setResponseError] = useState(undefined);
    const [loading, setLoading] = useState(false);

    const kushki = new Kushki({
        merchantId: KUSHKI_PUBLIC_MERCHANT_ID, // Your public merchant id
        inTestEnvironment: true,
    });


    const onSubmit = async (data) => {
        await getCardToken(data);
    }

    const getCardToken = (data) => {
        setLoading(true)
        setResponseError(undefined);
        setToken("");

        kushki.requestToken({
            amount: chargeAmount,
            currency: chargeCurrency,
            card: {
                name: data.cardName,
                number: data.cardNumber.replace(/ /g, ""),
                cvc: data.cvc,
                expiryMonth: data.expDate.split("/")[0],
                expiryYear: data.expDate.split("/")[1]
            },
        }, (response) => {
            if (!response.code) {
                setToken(response.token);
                makeCharge(response.token);
            } else {
                setLoading(false);
                setResponseError(response.message + '- Cliente');
            }
        });


    }

    const makeCharge = (token) => {

        axios.post("https://node-kushki-pkhwbz9p9-danyjos3.vercel.app/api/sendPaymentInformation", {
            "token": token,
            "chargeAmount": chargeAmount,
            "chargeCurrency": chargeCurrency
        },)
            .then((response) => {
                if (response.data.status === 400) {
                    setResponseError("Transacción declinada - Server")
                } else {
                    setResponseSuccess(response.data);
                }
            })
            .catch((error) => {
                if (error.response.data) {
                    setResponseError("Error en transacción");
                } else {
                    setResponseError("Error en transacción");
                }
            })
            .finally(() => {
                setLoading(false);
            });

    }

    const resetForm = () => {
        setResponseSuccess(undefined);
        setToken("");
        reset();
    };

    const validateDate = (value) => {
        if (value.indexOf("/") < 0) return false;
        const currentDate = new Date();
        const [month, year] = value.split("/");
        if (
            parseInt(year, 10) <
            parseInt(currentDate.getUTCFullYear().toString().substring(2), 10)
        ) {
            return false;
        } else if (
            parseInt(year, 10) ===
            parseInt(currentDate.getUTCFullYear().toString().substring(2), 10) &&
            parseInt(month, 10) < currentDate.getUTCMonth() + 1
        ) {
            return false;
        }
        return true;
    };

    return (
        <div>
            <Card className={styles.card}>
                <div className={styles.cardHeader}>
                    <img className={styles.logo} src={LogoKushki} alt="logoKushki"/>
                </div>
                {
                    responseSuccess ?
                        <CardContent>
                            <h3 className={styles.succesText}>Transacción Exitosa</h3>
                            <TextareaAutosize
                                className={styles.w100}
                                maxRows={20}
                                aria-label="maximum height"
                                defaultValue={JSON.stringify(responseSuccess, null, 2)}
                            />
                            <Button variant="contained" color="primary" onClick={resetForm}>
                                Realizar otro pago
                            </Button>
                        </CardContent>
                        :
                        <CardContent>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <Grid container spacing={3}>
                                    <Grid item xs={12}>
                                        <Controller
                                            name="cardNumber"
                                            control={control}
                                            rules={{
                                                required: 'Numero de tarjeta requerido',
                                                pattern: {
                                                    value: /[0-9| ]{16,20}/,
                                                    message: "Número de tarjeta incorrecto"
                                                }
                                            }}
                                            render={({field: {onChange, value}}) =>
                                                <InputMask
                                                    mask="9999 9999 9999 9999"
                                                    name="expDate"
                                                    value={value}
                                                    onChange={onChange}
                                                >
                                                    {(inputProps) => (
                                                        <TextField
                                                            {...inputProps}
                                                            className={styles.w100}
                                                            placeholder="Número de tarjeta"
                                                            InputProps={{
                                                                inputProps: {
                                                                    maxLength: 22,
                                                                }
                                                            }}
                                                        />
                                                    )}
                                                </InputMask>}


                                        />
                                        {errors.cardNumber ? errors.cardNumber.message : ""}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Controller
                                            name="cardName"
                                            control={control}
                                            rules={{required: 'Nombre requerido'}}
                                            render={({field}) =>
                                                <TextField {...field}
                                                           className={styles.w100}
                                                           placeholder="Nombre"
                                                />}
                                        />
                                        {errors.cardName ? errors.cardName.message : ""}
                                    </Grid>
                                    <Grid item xs={6}>

                                        <Controller
                                            name="expDate"
                                            control={control}
                                            rules={{
                                                required: 'Fecha expiración requerida',
                                                pattern: {
                                                    value: /0[123456789]|10|11|12\/[2-9][2-9]/,
                                                    message: "Fecha expiración no válida"
                                                }
                                            }}
                                            validate={(value) =>
                                                validateDate(value) || "Expiración vencida"}
                                            render={({field: {onChange, value}}) =>
                                                <InputMask
                                                    mask="99/99"
                                                    name="expDate"
                                                    value={value}
                                                    onChange={onChange}
                                                >
                                                    {(inputProps) => (
                                                        <TextField
                                                            {...inputProps}
                                                            placeholder='MM/YY'
                                                        />
                                                    )}
                                                </InputMask>}
                                        />

                                        {errors.expDate ? errors.expDate.message : ""}
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Controller
                                            name="cvc"
                                            control={control}
                                            rules={{
                                                required: 'CVC requerido',
                                                pattern: {
                                                    value: /[0-9]{3,4}/,
                                                    message: "CVC incorrecta"
                                                }
                                            }}

                                            render={({field}) =>
                                                <TextField {...field}
                                                           placeholder="CVC"
                                                           type={"password"}
                                                           InputProps={{
                                                               inputProps: {
                                                                   maxLength: 4,
                                                                   minLength: 3
                                                               }
                                                           }}
                                                />}
                                        />
                                        {errors.cvc ? errors.cvc.message : ""}
                                    </Grid>
                                    <Grid item xs={12}>
                                        {
                                            loading ?
                                                <div className={styles.center}>
                                                    <CircularProgress/>
                                                    <p>Procesando...</p>
                                                </div>
                                                :
                                                <Button
                                                    type="submit"
                                                    style={{fontSize: '20px', width: '100%'}}
                                                    variant="contained" color="primary"> Pagar $ {chargeAmount}</Button>
                                        }

                                    </Grid>
                                </Grid>
                            </form>
                        </CardContent>

                }

                {
                    token && <p>Token: {token}</p>
                }
                {
                    responseError && <p className={styles.error}>{responseError}</p>
                }

            </Card>

        </div>
    )
}
