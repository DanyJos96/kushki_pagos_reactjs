const express = require('express');
const cors = require('cors');
const axios = require('axios');


const app = express();
app.use(express.json());
app.use(cors());


app.get('/', (req, res) => {
    res.send('Node JS api')
});

app.post('/api/sendPaymentInformation', (req, res) => {

    //Transformar la data
    const data = JSON.stringify({
        token: req.body.token,
        amount: {
            subtotalIva: 0,
            subtotalIva0: req.body.chargeAmount,
            ice: 0,
            iva: 0,
            currency: req.body.chargeCurrency
        },
        fullResponse:true
    });

    //Realizar solicitud http
    axios({
        method: 'post',
        url: 'https://api-uat.kushkipagos.com/card/v1/charges',
        data: data,
        headers: {
            'Content-Type': 'application/json',
            'Private-Merchant-Id': '4c3859fe6033457ea1e9ffb31121bee8'
        }
    }).then((response) => {
        //Enviar la respuesta
        return res.send(JSON.stringify(response.data))
    })
        .catch((error) => {
            return res.send(JSON.stringify(error))
        })

});

const port = process.env.port || 80;
app.listen(port, () => console.log(`escuchando en el puerto ${port}`))


